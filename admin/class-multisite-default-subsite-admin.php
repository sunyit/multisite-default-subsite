<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://wasielewski.org
 * @since      1.0.0
 *
 * @package    Multisite_Default_Subsite
 * @subpackage Multisite_Default_Subsite/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Multisite_Default_Subsite
 * @subpackage Multisite_Default_Subsite/admin
 * @author     Zac Wasielewski <zac@wasielewski.org>
 */
class Multisite_Default_Subsite_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Multisite_Default_Subsite_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Multisite_Default_Subsite_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/multisite-default-subsite-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Multisite_Default_Subsite_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Multisite_Default_Subsite_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/multisite-default-subsite-admin.js', array( 'jquery' ), $this->version, false );

	}

	public function register_admin_settings() {
	  
	  add_settings_section( 'multisite-default-subsite','Default Blog','','general');	      
    add_settings_field(
      'redirect_to_blog',
      'Redirect To',
      array($this,'admin_setting_field_redirect_to_blog'),
      'general',
      'multisite-default-subsite',
      array( 'label_for' => 'redirect_to_blog' )
    );
    register_setting( 'general', 'redirect_to_blog' );

  }

  public function admin_setting_field_redirect_to_blog() {
    ?>
    <select name="redirect_to_blog" id="redirect_to_blog">
      <option value="">None</option>
      <?php
        foreach (wp_get_sites() as $site):
          $selected = (get_option('redirect_to_blog')==$site['blog_id']) ? 'selected' : '';
          $disabled = (get_current_blog_id()==$site['blog_id']) ? 'disabled' : '';
          ?>
          <option value="<?php echo $site['blog_id'] ?>" <?php echo $selected ?> <?php echo $disabled ?>>
            <?php echo $site['domain'] . $site['path'] ?>
          </option>
          <?php
        endforeach;
      ?>
	  </select>
	  <p class="description">All requests for the current blog will be forwarded to the selected blog.</p>
	  <?php
	}

}
