<?php

/**
 * Fired during plugin activation
 *
 * @link       http://wasielewski.org
 * @since      1.0.0
 *
 * @package    Multisite_Default_Subsite
 * @subpackage Multisite_Default_Subsite/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Multisite_Default_Subsite
 * @subpackage Multisite_Default_Subsite/includes
 * @author     Zac Wasielewski <zac@wasielewski.org>
 */
class Multisite_Default_Subsite_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
