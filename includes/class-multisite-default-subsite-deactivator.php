<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://wasielewski.org
 * @since      1.0.0
 *
 * @package    Multisite_Default_Subsite
 * @subpackage Multisite_Default_Subsite/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Multisite_Default_Subsite
 * @subpackage Multisite_Default_Subsite/includes
 * @author     Zac Wasielewski <zac@wasielewski.org>
 */
class Multisite_Default_Subsite_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
