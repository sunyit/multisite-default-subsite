<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://wasielewski.org
 * @since             1.0.0
 * @package           Multisite_Default_Subsite
 *
 * @wordpress-plugin
 * Plugin Name:       Multisite Default Subsite
 * Plugin URI:        https://bitbucket.org/wasielz/multisite-default-subsite
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Zac Wasielewski
 * Author URI:        http://wasielewski.org
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       multisite-default-subsite
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-multisite-default-subsite-activator.php
 */
function activate_multisite_default_subsite() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-multisite-default-subsite-activator.php';
	Multisite_Default_Subsite_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-multisite-default-subsite-deactivator.php
 */
function deactivate_multisite_default_subsite() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-multisite-default-subsite-deactivator.php';
	Multisite_Default_Subsite_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_multisite_default_subsite' );
register_deactivation_hook( __FILE__, 'deactivate_multisite_default_subsite' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-multisite-default-subsite.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_multisite_default_subsite() {

	$plugin = new Multisite_Default_Subsite();
	$plugin->run();

}
run_multisite_default_subsite();
