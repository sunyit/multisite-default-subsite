<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://wasielewski.org
 * @since      1.0.0
 *
 * @package    Multisite_Default_Subsite
 * @subpackage Multisite_Default_Subsite/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Multisite_Default_Subsite
 * @subpackage Multisite_Default_Subsite/public
 * @author     Zac Wasielewski <zac@wasielewski.org>
 */
class Multisite_Default_Subsite_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Multisite_Default_Subsite_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Multisite_Default_Subsite_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/multisite-default-subsite-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Multisite_Default_Subsite_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Multisite_Default_Subsite_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/multisite-default-subsite-public.js', array( 'jquery' ), $this->version, false );

	}

  public function redirect_to_sub_site() {
      global $wp;
      
    $redirect_to_blog_id = get_option('redirect_to_blog');
    if (empty($redirect_to_blog_id)) return;

    $redirect = get_blog_details($redirect_to_blog_id);

    $url = http_build_url('', array(
      "scheme" => ($_SERVER['HTTPS']==='on') ? 'https' : 'http',
      "host" => $redirect->domain,
      "path" => $redirect->path . $wp->request,
      "query" => (!empty($_SERVER['QUERY_STRING'])) ? $_SERVER['QUERY_STRING'] : NULL
    ));

    wp_redirect($url, 302);
      exit;
    }
}
